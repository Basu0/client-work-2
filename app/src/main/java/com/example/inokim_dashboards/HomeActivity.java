package com.example.inokim_dashboards;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class HomeActivity extends AppCompatActivity
         {

    String webAddress="https://datastudio.google.com/open/1F-2aydpPRAW_liKcObUL0Ae0cNB44UA3";

    WebView webView;
    FrameLayout frameLayout;
    ProgressBar progressBar;
    LinearLayout spisd,wedlid,fullout;
    LinearLayout l1,l2;
    Animation uptodown,downtoup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        webView=(WebView)findViewById(R.id.webV);
        frameLayout=(FrameLayout)findViewById(R.id.frameL);
        progressBar=(ProgressBar)findViewById(R.id.progressB);
        //spisd=(LinearLayout)findViewById(R.id.splashid);
        wedlid=(LinearLayout)findViewById(R.id.weblayoutid);
        fullout=(LinearLayout)findViewById(R.id.toolbarid);
       WebSettings webSettings = webView.getSettings();
        reload();


        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebViewClient(new HelpClient());
        webView.setWebChromeClient(new WebChromeClient()
        {
            public void onProgressChanged(WebView view,int newProgress){
                frameLayout.setVisibility(View.VISIBLE);
                progressBar.setProgress(newProgress);
                setTitle("Loading Please Wait....");
                if (newProgress==100)
                {   //spisd.setVisibility(View.GONE);
                    frameLayout.setVisibility(View.GONE);

                    //wedlid.setRotation(90.0f);
                    //toolbar.setRotation(90.0f);
                    wedlid.setVisibility(View.VISIBLE);

                    setTitle(view.getTitle());
                }

                super.onProgressChanged(view,newProgress);
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);

        if (haveNetworkConnection()){
            webView.loadUrl(webAddress);
        }else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        progressBar.setProgress(0);





    }

             /*@Override
             public void onBackPressed() {
                 DrawerLayout drawer = findViewById(R.id.drawer_layout);
                 if (drawer.isDrawerOpen(GravityCompat.START)) {
                     drawer.closeDrawer(GravityCompat.START);
                 } else {
                     super.onBackPressed();
                 }
             }*/

             @Override
             public boolean onCreateOptionsMenu(Menu menu) {
                 // Inflate the menu; this adds items to the action bar if it is present.
                 getMenuInflater().inflate(R.menu.home, menu);
                 return true;
             }

             @Override
             public boolean onOptionsItemSelected(MenuItem item) {
                 // Handle action bar item clicks here. The action bar will
                 // automatically handle clicks on the Home/Up button, so long
                 // as you specify a parent activity in AndroidManifest.xml.
                 int id = item.getItemId();

                 //noinspection SimplifiableIfStatement
                 if (id == R.id.action_settings) {
                     webView.reload();
                     return true;
                 }
                 if (id == R.id.home) {
                     webView.loadUrl(webAddress);
                     return true;
                 }

                 return super.onOptionsItemSelected(item);
             }

             @SuppressWarnings("StatementWithEmptyBody")
    /*@Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_tools) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*/

             private class HelpClient extends WebViewClient {
                 public boolean shouldOverrideUrlLoadding(WebView view ,String url){
                     view.loadUrl(url);
                     frameLayout.setVisibility(View.VISIBLE);
                     return true;
                 }
             }
             private boolean haveNetworkConnection(){
                 boolean haveConnectionWifi = false;
                 boolean haveConnectionMobile = false;

                 ConnectivityManager cm=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                 NetworkInfo[] networkInfos = cm.getAllNetworkInfo();

                 for (NetworkInfo networkInfo:networkInfos){
                     if (networkInfo.getTypeName().equalsIgnoreCase("WIFI"))
                         if (networkInfo.isConnected())
                             haveConnectionWifi =true;

                     if ( networkInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                         if (networkInfo.isConnected())
                             haveConnectionMobile =true;
                 }

                 return  haveConnectionWifi || haveConnectionMobile;

             }

             @Override
             public boolean onKeyDown(int keyCode, KeyEvent event) {

                 if ((keyCode==KeyEvent.KEYCODE_BACK) && webView.canGoBack()){
                     webView.goBack();
                     return  true;
                 }
                 return super.onKeyDown(keyCode, event);
             }

             public void reload() {
                 final Handler handler = new Handler();
                 handler.postDelayed(new Runnable() {
                     @Override
                     public void run() {
                         // Do something after 5min = 50000ms
                         webView.reload();
                         //Toast.makeText(HomeActivity.this, "Page Refresh ", Toast.LENGTH_SHORT).show();
                         reload();

                     }
                 }, 180000);
             }
         }